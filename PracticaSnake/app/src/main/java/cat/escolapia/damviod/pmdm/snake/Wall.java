package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by jordi.pages on 07/12/2016.
 */
public class Wall {

    public int x, y;

    public Wall(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getPosX() {

        return this.x;
    }

    public int getPosY() {

        return this.y;
    }
}
