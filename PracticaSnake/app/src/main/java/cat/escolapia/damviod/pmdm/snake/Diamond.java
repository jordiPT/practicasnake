package cat.escolapia.damviod.pmdm.snake;

public class Diamond {
    public static final int TYPE_1 = 0;
    public int x, y;
    public int type;

    public Diamond(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public int getPosX() {

        return this.x;
    }

    public int getPosY() {

        return this.y;
    }
}
